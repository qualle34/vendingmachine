var LoginModal = document.getElementById("loginForm");
var LoginBtn = document.getElementById("loginBtn");

LoginBtn.onclick = function () {
    LoginModal.style.display = "block";
    window.history.pushState("", "Login", "/login");
};

var RegistrationModal = document.getElementById("RegistrationForm");
var RegistrationBtn = document.getElementById("RegistrationBtn");

RegistrationBtn.onclick = function () {
    RegistrationModal.style.display = "block";
    window.history.pushState("", "Registration", "/registration");
};


var span1 = document.getElementsByClassName("login-close")[0];

span1.onclick = function () {
    LoginModal.style.display = "none";
};


var span2 = document.getElementsByClassName("registration-close")[0];

span2.onclick = function () {
    RegistrationModal.style.display = "none";
};


window.onclick = function (event) {
    if (event.target == LoginModal || event.target == RegistrationModal) {
        LoginModal.style.display = "none";
        RegistrationModal.style.display = "none";
    }
};