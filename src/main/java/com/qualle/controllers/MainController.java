package com.qualle.controllers;


import com.qualle.domain.User;
import com.qualle.domain.UserRole;
import com.qualle.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Collections;

@Controller
public class MainController {

    @GetMapping("/")
    public String LoadMainPage() {
        return "main";
    }

    @GetMapping("/private")
    public String LoadPrivatePage(){
        return "private";
    }

    @GetMapping("/registration")
    public String LoadRegistrationBox() {
        return "registration";
    }

    @Autowired
    private UserRepo userRepo;

    @PostMapping("/registration")
    public String addUser(User user, Model model) {
        User userFromDb = userRepo.findByUsername(user.getUsername());

        if (userFromDb != null) {
            model.addAttribute("message", "User exists!");
            return "registration";
        }

        user.setActive(true);
        user.setRoles(Collections.singleton(UserRole.USER));
        userRepo.save(user);

        return "redirect:/login";
    }

}
